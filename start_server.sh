#!/bin/bash
#

UNIVERSE=1
DMX_START=400

echo "starting up VLC and DMX Server."

vlc --extraintf http --http-host 127.0.0.1:8080&
ola_trigger -o $DMX_START -u $UNIVERSE vlc-remote.ola.conf
